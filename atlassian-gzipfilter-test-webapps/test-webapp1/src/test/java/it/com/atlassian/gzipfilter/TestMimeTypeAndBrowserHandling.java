package it.com.atlassian.gzipfilter;

import com.atlassian.gzipfilter.test.web.UTF8Servlet;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class TestMimeTypeAndBrowserHandling
{
    @Test
    public void testWebappDoesNotGzipWhenClientDoesNotSupportIt() throws IOException
    {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + "test.html");

        int i = client.executeMethod(method);
        assertEquals("Should return 200 - success", 200, i);
        IntegrationTestUtils.assertNonGzipped(method);
    }

    @Test
    public void testWebappDoesGzipAndContentIsCorrect() throws IOException
    {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + "test.html");
        method.addRequestHeader(IntegrationTestUtils.GZIP_ACCEPT_HEADER);
        int i = client.executeMethod(method);
        assertEquals("Should return 200 - success", 200, i);
        IntegrationTestUtils.assertGzipped(method);
        assertTrue("Content does not match." +
                        "Should be '" + new String(gzipContent("<h1>Test Servlet</h1>"), "UTF-8")
                        + "' but was '" + new String(method.getResponseBody(), "UTF-8") + "'",
                Arrays.equals(gzipContent("<h1>Test Servlet</h1>"), method.getResponseBody()));
    }

    @Test
    public void testWebappOnlyGzipsCertainMimeTypes() throws IOException
    {
        // test mime-types that should be gzipped
        assertGzipped("text/plain", true);
        assertGzipped("text/html", true);
        assertGzipped("application/javascript", true);
        assertGzipped("application/xhtml+xml", true);
        assertGzipped("application/xml", true);

        // test mime-types that shouldn't be gzipped
        assertGzipped("application/octet-stream", false);
        assertGzipped("image/png", false);
        assertGzipped("image/jpeg", false);
        assertGzipped("image/jpg", false);
    }

    @Test
    public void testWebappOnlyGzipsCertainMimeTypesForCertainBrowsers() throws IOException
    {
        final String IE_6 = "Mozilla/4.0 (compatible; MSIE 6.0b; Windows NT 5.1)";
        final String FIREFOX_3 = "Mozilla/6.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:2.0.0.0) Gecko/20061028 Firefox/3.0";

        assertGzipped("text/css", IE_6, false);
        assertGzipped("text/html", IE_6, false);
        assertGzipped("text/javascript", IE_6, false);

        assertGzipped("text/css", FIREFOX_3, true);
        assertGzipped("text/html", FIREFOX_3, true);
        assertGzipped("text/javascript", FIREFOX_3, true);
    }

    @Test
    public void testUTF8() throws IOException
    {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + "utf8.html");
        method.addRequestHeader(IntegrationTestUtils.GZIP_ACCEPT_HEADER);
        int i = client.executeMethod(method);
        assertEquals("Should return 200 - success", 200, i);
        IntegrationTestUtils.assertGzipped(method);
        String expectedText = "<h1>Test Servlet: '" + UTF8Servlet.UTF8_TEXT + "' </h1>";
        assertTrue("Content does not match.  Should be '" + expectedText + "' but was '" + gunzipContent(method.getResponseBody(), "UTF-8") + "'",
                Arrays.equals(gzipContent(expectedText), method.getResponseBody()));
    }

    private void assertGzipped(String mimeType, boolean shouldGzip) throws IOException
    {
        assertGzipped(mimeType, null, shouldGzip);
    }

    private void assertGzipped(final String mimeType, final String userAgent, final boolean shouldGzip)
            throws IOException
    {
        IntegrationTestUtils.assertPathGzipped("test.html?mimetype=" + URLEncoder.encode(mimeType, "UTF-8"), userAgent, shouldGzip);
    }

    private byte[] gzipContent(String content) throws IOException
    {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GZIPOutputStream outputStream = new GZIPOutputStream(bao);
        outputStream.write(content.getBytes("UTF-8"));
        outputStream.close();
        return bao.toByteArray();
    }

    private String gunzipContent(byte[] content, String mimeType) throws IOException
    {
        ByteArrayInputStream bai = new ByteArrayInputStream(content);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GZIPInputStream inputStream = new GZIPInputStream(bai);
        int i;
        while ((i = inputStream.read()) != -1)
        {
            bao.write(i);
        }
        return new String(bao.toByteArray(), mimeType);
    }
}