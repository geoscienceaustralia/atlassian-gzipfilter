package it.com.atlassian.gzipfilter;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Ignore;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * An utility class to facilitate testing
 */
class IntegrationTestUtils
{
    static final String HTTP_PORT = System.getProperty("http.port", "8080");
    static final String URL = "http://localhost:" + HTTP_PORT + "/test-webapp1/";

    static final Header GZIP_RESPONSE_HEADER = new Header("Content-Encoding", "gzip");
    static final Header GZIP_ACCEPT_HEADER = new Header("Accept-Encoding", "gzip");

    static HttpMethod assertPathGzipped(String path, String userAgent, boolean shouldGzip) throws IOException
    {
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + path);
        method.addRequestHeader(IntegrationTestUtils.GZIP_ACCEPT_HEADER);
        if (userAgent != null)
            method.addRequestHeader("User-Agent", userAgent);

        int i = client.executeMethod(method);
        assertEquals("Should return 200 - success", 200, i);
        if (shouldGzip)
            assertGzipped(method);
        else
            assertNonGzipped(method);
        return method;
    }

    static void assertGzipped(HttpMethod method)
    {
        assertEquals("Should return gzipped content", IntegrationTestUtils.GZIP_RESPONSE_HEADER, method.getResponseHeader("Content-Encoding"));
    }

    static void assertNonGzipped(HttpMethod method)
    {
        Header responseHeader = method.getResponseHeader("Content-Encoding");
        if (responseHeader == null)
            return;
        else if (responseHeader.getValue() == null)
            return;
        else if (responseHeader.getValue().length() == 0) {
            fail("Content-Encoding header can't be empty");
        }
        fail("ReponseHeader should be empty but was " + responseHeader.getValue());
    }
}
