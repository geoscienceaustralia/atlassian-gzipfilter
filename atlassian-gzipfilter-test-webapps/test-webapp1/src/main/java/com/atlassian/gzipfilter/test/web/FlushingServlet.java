package com.atlassian.gzipfilter.test.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * writes output, pauses, then writes more output
 * the caller should be able to detect the pause
 */
public class FlushingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long delay = 10000;
        if (req.getParameter("delay") != null) {
            delay = Long.parseLong(req.getParameter("delay"));
        }
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        out.print("first");
        out.flush();
        sleep(delay);
        out.print("second");
        out.flush();
        sleep(delay);
        out.print("last");
    }

    private static void sleep(long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
