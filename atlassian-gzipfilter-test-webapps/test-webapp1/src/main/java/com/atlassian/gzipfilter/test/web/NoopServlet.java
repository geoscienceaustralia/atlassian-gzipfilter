package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A noop servlet to use with HeaderRewriterFilter
 */
public class NoopServlet extends HttpServlet
{
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    }
}