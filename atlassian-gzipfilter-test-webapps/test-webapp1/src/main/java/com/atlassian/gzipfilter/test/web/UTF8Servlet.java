package com.atlassian.gzipfilter.test.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * A servlet that send UTF-8 text back to the client.
 */
public class UTF8Servlet extends HttpServlet
{
    public static final String UTF8_TEXT = "\u0126\u0118\u0139\u0139\u0150";

    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final PrintWriter printWriter = resp.getWriter();
        String mimeType = req.getParameter("encoding");
        resp.setContentType("text/html;charset=" + (mimeType != null ? mimeType : "UTF-8"));
        printWriter.write("<h1>Test Servlet: '" + UTF8_TEXT + "' </h1>");
    }

}
