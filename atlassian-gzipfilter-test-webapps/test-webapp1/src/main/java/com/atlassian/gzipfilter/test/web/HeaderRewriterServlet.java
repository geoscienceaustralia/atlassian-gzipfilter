package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A servlet to test gzip filter when content-type header gets rewritten
 */
public class HeaderRewriterServlet extends HttpServlet
{
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        ContentTypeRewriter.serviceRequest(req, resp);
    }
}