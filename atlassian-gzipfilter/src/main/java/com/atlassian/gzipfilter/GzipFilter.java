package com.atlassian.gzipfilter;

import java.io.File;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.gzipfilter.integration.GzipFilterIntegration;
import com.atlassian.gzipfilter.selector.GzipCompatibilitySelector;
import com.atlassian.gzipfilter.selector.GzipCompatibilitySelectorFactory;
import com.atlassian.gzipfilter.selector.NoGzipCompatibilitySelector;
import com.atlassian.gzipfilter.selector.UserAgentBasedGzipSelectorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter works in conjunction with a UserAgentBasedGzipSelectorFactory to determine whether to return noop
 * selector or gzip-ing one. It relies on UserAgentBasedGzipSelectorFactory which check request browser and response
 * mime types.
 * <p/>
 * Default gzippable mime types are set to {@link com.atlassian.gzipfilter.selector.UserAgentBasedGzipSelectorFactory#DEFAULT_COMPRESSABLE_MIME_TYPES}.
 * <p/>
 * Default non-gzippable user agents are set to {@link com.atlassian.gzipfilter.selector.UserAgentBasedGzipSelectorFactory#NO_COMPRESSION_USER_AGENTS_PARAM_NAME}.
 * <p/>
 * It's possible to override any of these settings via {@link com.atlassian.gzipfilter.selector.UserAgentBasedGzipSelectorFactory#COMPRESSABLE_MIME_TYPES_PARAM_NAME}
 * {@link com.atlassian.gzipfilter.selector.UserAgentBasedGzipSelectorFactory#NO_COMPRESSION_USER_AGENTS_PARAM_NAME}
 * init params respectively
 * <p/>
 * <p/>
 * Please note that historically gzip check was based on url-rewrite and this will now check for the existence of
 * /WEB-INF/urlrewrite-gzip.xml or urlrewrite.configfile init param in order to fail fast if any are found.
 */
public class GzipFilter extends AbstractFilter
{
    private static final Logger log = LoggerFactory.getLogger(GzipFilter.class);
    private static final String ALREADY_FILTERED = GzipFilter.class.getName() + "_already_filtered";

    private static final GzipCompatibilitySelector NO_GZIP_SELECTOR = new NoGzipCompatibilitySelector();

    private GzipCompatibilitySelectorFactory factory;
    private final GzipFilterIntegration integration;
    private FilterConfig filterConfig;

    protected static final String LEGACY_INIT_PARAM = "urlrewrite.configfile";

    protected static final String LEGACY_CONFIG_FILE = "urlrewrite-gzip.xml";

    public GzipFilter(GzipFilterIntegration integration)
    {
        this.integration = integration;
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {

        if (filterConfig.getInitParameter(LEGACY_INIT_PARAM) != null)
        {
            throw new IllegalArgumentException("Url rewrite is no longer used in gzip filter, "
                    + "you provided " + LEGACY_INIT_PARAM + " as init param");
        }

        final ServletContext servletContext = filterConfig.getServletContext();
        if (servletContext != null)
        {
            final String path = servletContext.getRealPath("/WEB-INF/" + LEGACY_CONFIG_FILE);
            if (new File(path).exists())
            {
                throw new IllegalArgumentException("Url rewrite is no longer used in gzip filter, "
                        + "but you have " + LEGACY_CONFIG_FILE + " in web-inf directory");
            }

        }

        this.filterConfig = filterConfig;
        this.factory = new UserAgentBasedGzipSelectorFactory(filterConfig);
        super.init(filterConfig);
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException
    {
        if (req.getAttribute(ALREADY_FILTERED) == null)
        {
            doFilterInternal(req, res, chain);
        }
        else
        {
            chain.doFilter(req, res);
        }
    }

    private void doFilterInternal(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException
    {
        req.setAttribute(ALREADY_FILTERED, Boolean.TRUE);

        if (req instanceof HttpServletRequest && integration.useGzip() && isTopLevelRequest(req))
        {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;
            GzipCompatibilitySelector selector = getCompatibilitySelector(request);
            if (selector.shouldGzip())
            {
                log.debug("GZIP supported, compressing.");

                SelectingResponseWrapper wrappedResponse = new SelectingResponseWrapper(response, selector, integration.getResponseEncoding(request));
                chain.doFilter(req, wrappedResponse);
                wrappedResponse.finishResponse();
                return;
            }
        }

        chain.doFilter(req, res);
    }

    private GzipCompatibilitySelector getCompatibilitySelector(HttpServletRequest request)
    {
        String acceptEncodingHeader = request.getHeader("accept-encoding");
        if (acceptEncodingHeader == null || acceptEncodingHeader.indexOf("gzip") == -1)
        {
            return NO_GZIP_SELECTOR;
        }

        return getFactory().getSelector(filterConfig, request);
    }

    protected GzipCompatibilitySelectorFactory getFactory()
    {
        return factory;
    }

    private boolean isTopLevelRequest(ServletRequest request)
    {
        return request.getAttribute("javax.servlet.include.servlet_path") == null;
    }

}
