package com.atlassian.gzipfilter.selector;

public interface GzipCompatibilitySelector
{
    public boolean shouldGzip(String contentType);

    public boolean shouldGzip();

}
