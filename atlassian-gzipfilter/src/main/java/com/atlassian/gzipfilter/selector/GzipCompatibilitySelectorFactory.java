package com.atlassian.gzipfilter.selector;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;

public interface GzipCompatibilitySelectorFactory
{
    public GzipCompatibilitySelector getSelector(final FilterConfig filterConfig, final HttpServletRequest request);

}
