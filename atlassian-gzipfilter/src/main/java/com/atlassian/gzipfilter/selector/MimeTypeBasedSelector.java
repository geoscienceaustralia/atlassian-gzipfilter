package com.atlassian.gzipfilter.selector;

import java.util.Set;

public class MimeTypeBasedSelector implements GzipCompatibilitySelector
{
    private final Set<String> compressableMimeTypes;

    public MimeTypeBasedSelector(Set<String> compressableMimeTypes)
    {
        this.compressableMimeTypes = compressableMimeTypes;
    }

    public boolean shouldGzip(String contentType)
    {
        return compressableMimeTypes.contains(contentType);
    }

    public boolean shouldGzip()
    {
        return true;
    }
}
