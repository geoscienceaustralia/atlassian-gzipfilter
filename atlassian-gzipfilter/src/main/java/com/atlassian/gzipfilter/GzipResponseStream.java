package com.atlassian.gzipfilter;

import com.atlassian.gzipfilter.util.FlushableGZIPOutputStreamFactory;

import java.io.IOException;
import java.util.zip.GZIPOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public final class GzipResponseStream extends ServletOutputStream
{
    protected final GZIPOutputStream gzipstream;
    protected final HttpServletResponse response;

    private final static int DEFAULT_BUFFER_SIZE_BYTES = 1024;

    public GzipResponseStream(HttpServletResponse response) throws IOException
    {
        super();
        this.response = response;
        gzipstream = FlushableGZIPOutputStreamFactory.makeFlushableGZIPOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE_BYTES);
    }

    public void close() throws IOException
    {
        gzipstream.close();
    }

    public void flush() throws IOException
    {
        gzipstream.flush();
    }

    public void write(int b) throws IOException
    {
        gzipstream.write(b);
    }

    public void write(byte b[]) throws IOException
    {
        this.write(b, 0, b.length);
    }

    public void write(byte b[], int off, int len) throws IOException
    {
        gzipstream.write(b, off, len);
    }
}
